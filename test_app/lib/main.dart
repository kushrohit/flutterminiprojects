import 'package:flutter/material.dart';

void main(){
  runApp(const MyApp());
}


// Type of widget
// 1. StatelessWidget : for UI (Immutable data, not change the data in widget)
// 2. StatefulWidget   for UI  (Mutable data, change the data in widget)
// 3. InheritedWidget

// State (data, how to looklike and how to run and handled by widget)

// UI design system

// Material Design (android)
// Coupertino Design (ios,mac)

class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
  //   return const Text('Hello World !!!!',
  // textDirection: TextDirection.rtl,
  // );

    return const MaterialApp(
      home: Text('Hello World !!!'),
    );
  }

}